package inf226.util.immutable;

public final class Password {

    private final String password;

    public Password(String password) throws IllegalArgumentException {
        if (isValid(password)) {
            this.password = password;
        } else {
            throw new IllegalArgumentException();
        }
    }

    public String get() {
        return password;
    }

    public boolean isValid(String password) {
        if (password.length() < 8 || password.length() > 1000) {
            return false;
        }

        return true;
    }
}
