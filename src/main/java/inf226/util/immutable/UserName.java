package inf226.util.immutable;

public final class UserName {

    private final String name;

    public UserName(String username) {
        this.name = username;
    }

    public String get() {
        return this.name;
    }
}
