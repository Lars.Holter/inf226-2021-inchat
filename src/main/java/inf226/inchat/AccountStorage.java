package inf226.inchat;

import java.sql.*;
import java.time.Instant;
import java.util.UUID;

import inf226.storage.*;

import inf226.util.immutable.List;
import inf226.util.*;
import inf226.util.immutable.UserName;

/**
 * This class stores accounts in the database.
 */
public final class AccountStorage
        implements Storage<Account,SQLException> {

    final Connection connection;
    final Storage<User,SQLException> userStore;
    final Storage<Channel,SQLException> channelStore;

    /**
     * Create a new account storage.
     *
     * @param  connection   The connection to the SQL database.
     * @param  userStore    The storage for User data.
     * @param  channelStore The storage for channels.
     */
    public AccountStorage(Connection connection,
                          Storage<User,SQLException> userStore,
                          Storage<Channel,SQLException> channelStore)
            throws SQLException {
        this.connection = connection;
        this.userStore = userStore;
        this.channelStore = channelStore;

        connection.createStatement()
                .executeUpdate("CREATE TABLE IF NOT EXISTS Account (id TEXT PRIMARY KEY, version TEXT, user TEXT, password TEXT, FOREIGN KEY(user) REFERENCES User(id) ON DELETE CASCADE)");
        connection.createStatement()
                .executeUpdate("CREATE TABLE IF NOT EXISTS AccountChannel (account TEXT, channel TEXT, alias TEXT, ordinal INTEGER, role TEXT, PRIMARY KEY(account,channel), FOREIGN KEY(account) REFERENCES Account(id) ON DELETE CASCADE, FOREIGN KEY(channel) REFERENCES Channel(id) ON DELETE CASCADE)");
    }

    @Override
    public Stored<Account> save(Account account)
            throws SQLException {

        final Stored<Account> stored = new Stored<Account>(account);
        String sql = "INSERT INTO Account VALUES(?, ?, ?, ?)";

        final PreparedStatement prepared = connection.prepareStatement(sql);
        prepared.setObject(1, stored.identity);
        prepared.setObject(2, stored.version);
        prepared.setObject(3, account.user.identity);
        prepared.setString(4, account.hashedPassword);
        prepared.execute();

        // Write the list of channels
        final Maybe.Builder<SQLException> exception = Maybe.builder();
        final Mutable<Integer> ordinal = new Mutable<Integer>(0);
        account.channels.forEach(element -> {
            String alias = element.first;
            Stored<Channel> channel = element.second;
            Role role = element.third;
            final String msql = "INSERT INTO AccountChannel VALUES(?, ?, ?, ?, ?)";
            try {
                final PreparedStatement preparedChannel = connection.prepareStatement(msql);
                preparedChannel.setObject(1, stored.identity);
                preparedChannel.setObject(2, channel.identity);
                preparedChannel.setString(3, alias);
                preparedChannel.setString(4, ordinal.get().toString());
                preparedChannel.setString(5, role.toString());
                preparedChannel.execute();
            }
            catch (SQLException e) { exception.accept(e) ; }
            ordinal.accept(ordinal.get() + 1);
        });

        Util.throwMaybe(exception.getMaybe());
        return stored;
    }

    @Override
    public synchronized Stored<Account> update(Stored<Account> account,
                                               Account new_account)
            throws UpdatedException,
            DeletedException,
            SQLException {
        final Stored<Account> current = get(account.identity);
        final Stored<Account> updated = current.newVersion(new_account);
        if(current.version.equals(account.version)) {
            String asql = "UPDATE Account SET (version,user) = (?, ?) WHERE id=?";

            final PreparedStatement preparedAccount = connection.prepareStatement(asql);
            preparedAccount.setObject(1, updated.version);
            preparedAccount.setObject(2, new_account.user.identity);
            preparedAccount.setObject(3, updated.identity);
            preparedAccount.execute();


            // Rewrite the list of channels
            String acsql = "DELETE FROM AccountChannel WHERE account=?";
            final PreparedStatement preparedAccountChannel = connection.prepareStatement(acsql);
            preparedAccountChannel.setObject(1, account.identity);
            preparedAccountChannel.execute();

            final Maybe.Builder<SQLException> exception = Maybe.builder();
            final Mutable<Integer> ordinal = new Mutable<Integer>(0);
            new_account.channels.forEach(element -> {
                String alias = element.first;
                Stored<Channel> channel = element.second;
                Role role = element.third;
                final String msql
                        = "INSERT INTO AccountChannel VALUES(?, ?, ?, ?, ?)";
                try {
                    final PreparedStatement preparedChannel = connection.prepareStatement(msql);
                    preparedChannel.setObject(1, account.identity);
                    preparedChannel.setObject(2, channel.identity);
                    preparedChannel.setString(3, alias);
                    preparedChannel.setString(4, ordinal.get().toString());
                    preparedChannel.setString(5, role.toString());
                    preparedChannel.execute();
                }
                catch (SQLException e) { exception.accept(e) ; }
                ordinal.accept(ordinal.get() + 1);
            });

            Util.throwMaybe(exception.getMaybe());
        } else {
            throw new UpdatedException(current);
        }
        return updated;
    }

    @Override
    public synchronized void delete(Stored<Account> account)
            throws UpdatedException,
            DeletedException,
            SQLException {
        final Stored<Account> current = get(account.identity);
        if(current.version.equals(account.version)) {
            String sql =  "DELETE FROM Account WHERE id =?";

            final PreparedStatement prepared = connection.prepareStatement(sql);
            prepared.setObject(1, account.identity);
            prepared.execute();

        } else {
            throw new UpdatedException(current);
        }
    }
    @Override
    public Stored<Account> get(UUID id)
            throws DeletedException,
            SQLException {

        final String accountsql = "SELECT version,user,password FROM Account WHERE id = ?";
        final String channelsql = "SELECT channel,alias,ordinal,role FROM AccountChannel WHERE account = ? ORDER BY ordinal DESC";

        final PreparedStatement accountPrepared = connection.prepareStatement(accountsql);
        final PreparedStatement channelPrepared = connection.prepareStatement(channelsql);
        accountPrepared.setObject(1, id);
        channelPrepared.setObject(1, id);

        final ResultSet accountResult = accountPrepared.executeQuery();
        final ResultSet channelResult = channelPrepared.executeQuery();

        if(accountResult.next()) {
            final UUID version = UUID.fromString(accountResult.getString("version"));
            final UUID userid =
                    UUID.fromString(accountResult.getString("user"));
            final String password =
                    accountResult.getString("password");
            final Stored<User> user = userStore.get(userid);
            // Get all the channels associated with this account
            final List.Builder<Triple<String,Stored<Channel>,Role>> channels = List.builder();
            while(channelResult.next()) {
                final UUID channelId =
                        UUID.fromString(channelResult.getString("channel"));
                final String alias = channelResult.getString("alias");
                final Role role = Role.valueOf(channelResult.getString("role"));
                channels.accept(
                        new Triple<String,Stored<Channel>,Role>(
                                alias,channelStore.get(channelId),role));
            }
            return (new Stored<Account>(new Account(user,channels.getList(),password),id,version));
        }

        throw new DeletedException();
    }

    /**
     * Look up an account based on their username.
     */
    public Stored<Account> lookup(UserName username)
            throws DeletedException,
            SQLException {
        final String sql = "SELECT Account.id from Account INNER JOIN User ON user=User.id where User.name=?";

        PreparedStatement prepared = connection.prepareStatement(sql);
        prepared.setString(1, username.get());

        System.err.println(sql);

        final ResultSet rs = prepared.executeQuery();
        if (rs.next()) {
            final UUID identity =
                    UUID.fromString(rs.getString("id"));
            return get(identity);
        }

        throw new DeletedException();
    }

}
