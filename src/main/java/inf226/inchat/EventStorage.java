package inf226.inchat;

import java.sql.*;
import java.time.Instant;
import java.util.UUID;
import java.util.function.Consumer;

import inf226.storage.*;
import inf226.util.*;




public final class EventStorage
    implements Storage<Channel.Event,SQLException> {
    
    private final Connection connection;
    
    public EventStorage(Connection connection) 
      throws SQLException {
        this.connection = connection;
        connection.createStatement()
                .executeUpdate("CREATE TABLE IF NOT EXISTS Event (id TEXT PRIMARY KEY, version TEXT, channel TEXT, type INTEGER, time TEXT, FOREIGN KEY(channel) REFERENCES Channel(id) ON DELETE CASCADE)");
        connection.createStatement()
                .executeUpdate("CREATE TABLE IF NOT EXISTS Message (id TEXT PRIMARY KEY, sender TEXT, content Text, FOREIGN KEY(id) REFERENCES Event(id) ON DELETE CASCADE)");
        connection.createStatement()
                .executeUpdate("CREATE TABLE IF NOT EXISTS Joined (id TEXT PRIMARY KEY, sender TEXT, FOREIGN KEY(id) REFERENCES Event(id) ON DELETE CASCADE)");
    }
    
    @Override
    public Stored<Channel.Event> save(Channel.Event event)
      throws SQLException {
        
        final Stored<Channel.Event> stored = new Stored<Channel.Event>(event);

        String sql =  "INSERT INTO Event VALUES(?, ?, ?, ?, ?)";
        final PreparedStatement preparedEvent = connection.prepareStatement(sql);
        preparedEvent.setObject(1, stored.identity);
        preparedEvent.setObject(2, stored.version);
        preparedEvent.setObject(3, event.channel);
        preparedEvent.setInt(4, event.type.code);
        preparedEvent.setObject(5, event.time);
        preparedEvent.execute();

        switch (event.type) {
            case message:
                sql = "INSERT INTO Message VALUES(?, ?, ?)";
                final PreparedStatement preparedMessage = connection.prepareStatement(sql);
                preparedMessage.setObject(1, stored.identity);
                preparedMessage.setString(2, event.sender);
                preparedMessage.setString(3, event.message);
                preparedMessage.execute();
                break;
            case join:
                sql = "INSERT INTO Joined VALUES(?, ?)";
                final PreparedStatement preparedJoined = connection.prepareStatement(sql);
                preparedJoined.setObject(1, stored.identity);
                preparedJoined.setString(2, event.sender);
                preparedJoined.execute();
                break;
        }
        return stored;
    }
    
    @Override
    public synchronized Stored<Channel.Event> update(Stored<Channel.Event> event,
                                            Channel.Event new_event)
        throws UpdatedException,
            DeletedException,
            SQLException {
    final Stored<Channel.Event> current = get(event.identity);
    final Stored<Channel.Event> updated = current.newVersion(new_event);
    if(current.version.equals(event.version)) {
        String sql = "UPDATE Event SET (version,channel,time,type) =(?, ?, ?, ?) WHERE id=?";

        final PreparedStatement preparedEvent = connection.prepareStatement(sql);
        preparedEvent.setObject(1, updated.version);
        preparedEvent.setObject(2, new_event.channel);
        preparedEvent.setObject(3, new_event.time);
        preparedEvent.setInt(4, new_event.type.code);
        preparedEvent.setObject(5, updated.identity);
        preparedEvent.execute();

        switch (new_event.type) {
            case message:
                sql = "UPDATE Message SET (sender,content)=(?, ?) WHERE id=?";
                final PreparedStatement preparedMessage = connection.prepareStatement(sql);
                preparedMessage.setString(1, new_event.sender);
                preparedMessage.setString(2, new_event.message);
                preparedMessage.setObject(3, updated.identity);
                preparedMessage.execute();
                break;
            case join:
                sql = "UPDATE Joined SET (sender)=(?) WHERE id=?";
                final PreparedStatement preparedJoined = connection.prepareStatement(sql);
                preparedJoined.setString(1, new_event.sender);
                preparedJoined.setObject(2, updated.identity);
                preparedJoined.execute();
                break;
        }
    } else {
        throw new UpdatedException(current);
    }
    return updated;
    }
   
    @Override
    public synchronized void delete(Stored<Channel.Event> event)
       throws UpdatedException,
              DeletedException,
              SQLException {
        final Stored<Channel.Event> current = get(event.identity);
        if(current.version.equals(event.version)) {
            String sql =  "DELETE FROM Event WHERE id =?";

            final PreparedStatement prepared = connection.prepareStatement(sql);
            prepared.setObject(1, event.identity);
            prepared.execute();
        }
        else {
            throw new UpdatedException(current);
        }
    }
    @Override
    public Stored<Channel.Event> get(UUID id)
      throws DeletedException,
             SQLException {
        final String sql = "SELECT version,channel,time,type FROM Event WHERE id = ?";

        final PreparedStatement prepared = connection.prepareStatement(sql);
        prepared.setObject(1, id);

        final ResultSet rs = prepared.executeQuery();

        if(rs.next()) {
            final UUID version = UUID.fromString(rs.getString("version"));
            final UUID channel = 
                UUID.fromString(rs.getString("channel"));
            final Channel.Event.Type type = 
                Channel.Event.Type.fromInteger(rs.getInt("type"));
            final Instant time = 
                Instant.parse(rs.getString("time"));
            
            final Statement mstatement = connection.createStatement();
            switch(type) {
                case message:
                    final String msql = "SELECT sender,content FROM Message WHERE id = ?";
                    final PreparedStatement preparedMessage = connection.prepareStatement(msql);
                    preparedMessage.setObject(1, id);

                    final ResultSet mrs = preparedMessage.executeQuery();
                    mrs.next();
                    return new Stored<Channel.Event>(
                            Channel.Event.createMessageEvent(channel,time,mrs.getString("sender"),mrs.getString("content")),
                            id,
                            version);
                case join:
                    final String asql = "SELECT sender FROM Joined WHERE id = ?";
                    final PreparedStatement preparedJoined = connection.prepareStatement(asql);
                    preparedJoined.setObject(1, id);

                    final ResultSet ars = preparedJoined.executeQuery();
                    ars.next();
                    return new Stored<Channel.Event>(
                            Channel.Event.createJoinEvent(channel,time,ars.getString("sender")),
                            id,
                            version);
            }
        }
        throw new DeletedException();
    }
    
}


 
